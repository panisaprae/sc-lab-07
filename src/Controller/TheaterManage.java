package Controller;

import java.util.ArrayList;
import java.util.Scanner;

import Model.CostManager;



public class TheaterManage {
	ArrayList<Integer> arrayList = new ArrayList<Integer>();
	static int[][] ticketPrices1;
	
	public TheaterManage(CostManager dataSeatPriceManager){
		ticketPrices1 = dataSeatPriceManager.getDataSeatPrice();
	}
	
	public int[][] changeAvailability(int i,int j)
	{
		ticketPrices1[i][j] = 0;
		return ticketPrices1;
	}
	
	public boolean seatAvailable(int i,int j){
		if (ticketPrices1[i][j] != 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public int getPrice(int i, int j){
		int price = ticketPrices1[i][j];
		return price;
	}
	public ArrayList<Integer> add(int i, int j){
		arrayList.add(ticketPrices1[i][j]);
		return arrayList;
	}
	public int totalPrice(){
		
		int sum = 0;
		for (int count = 0; count < arrayList.size(); count++) {
			sum += arrayList.get(count);
		}
		return sum;
	}
	
	public String showAvailableSeatsByPrice(int price)
	{
		//show seats that are user-entered price
		String empty="";
		for(int i = 1; i < ticketPrices1.length; i++)
		{    //searches array for matching prices
			for(int j = 1; j < ticketPrices1[i].length; j++)
			{
				if(ticketPrices1[i][j] == price)
				{
					empty += "[" + i + "," + j + "]" + " ";
				}
			}
		}
		return empty;
	}
}


