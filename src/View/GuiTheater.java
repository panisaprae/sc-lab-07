package View;

import Controller.TheaterManage;
import Model.CostSeat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class GuiTheater extends JFrame {
	private JPanel buttonpanel;
	private JLabel[][] buttonlabel;
	private JPanel choosepanel;
	private JTextField txtrow;
	private JTextField txtclomn;
	private JLabel rowbelabel;
	private JLabel clomnlabel;
	private JPanel allpanel;
	private JButton submit;
	private JLabel pricelabel;
	private JPanel pricepenel;
	private JTextField txtprice;
	private JPanel southpanel;
	private JButton seach;
	private JLabel totallabel;
	private JLabel totalpricelabel;
	private JPanel northpanel;
	CostSeat dataSeatPrice = new CostSeat();
	TheaterManage theaterManagement = new TheaterManage(dataSeatPrice);
	private int j;
	private int i;
	private int row;
	private int column;
	protected Integer price;

	public GuiTheater() {

		setTitle("");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(1000, 600);
		setResizable(true);

		panel();
	}

	public void panel() {
		final String[][] ticketPrices = {
				{ "", " 1", " 2", " 3", " 4", " 5", " 6", " 7", " 8", " 9",
						"10", "11", "12", "13", "14", "15", "16", "17", "18",
						"19", "20" }, // 1-20
				{ "A", "  ", "  ", "10", "10", "10", "10", "10", "10", "  ",
						"  ", "10", "10", "10", "10", "10", "10", "10", "10",
						"  ", "  " }, // a
				{ "B", "  ", "10", "10", "10", "10", "10", "10", "10", "  ",
						"  ", "10", "10", "10", "10", "10", "10", "10", "10",
						"10", "  " }, // b
				{ "C", "10", "10", "10", "10", "10", "10", "10", "10", "  ",
						"  ", "10", "10", "10", "10", "10", "10", "10", "10",
						"10", "10" }, // c
				{ "D", "10", "10", "20", "20", "20", "20", "20", "20", "  ",
						"  ", "20", "20", "20", "20", "20", "20", "20", "20",
						"10", "10" }, // d
				{ "E", "10", "10", "20", "20", "20", "20", "20", "20", "  ",
						"  ", "20", "20", "20", "20", "20", "20", "20", "20",
						"10", "10" }, // e
				{ "F", "10", "10", "20", "20", "20", "20", "20", "20", "  ",
						"  ", "20", "20", "20", "20", "20", "20", "20", "20",
						"10", "10" }, // f
				{ "G", "20", "20", "20", "30", "30", "30", "30", "30", "  ",
						"  ", "30", "30", "30", "30", "30", "30", "30", "20",
						"20", "20" }, // g
				{ "H", "30", "30", "30", "40", "40", "40", "40", "40", "  ",
						"  ", "40", "40", "40", "40", "40", "40", "40", "30",
						"30", "30" }, // h
				{ "I", "30", "30", "30", "40", "40", "40", "40", "40", "  ",
						"  ", "40", "40", "40", "40", "40", "40", "40", "30",
						"30", "30" }, // i
				{ "L", "30", "30", "30", "40", "40", "40", "40", "40", "  ",
						"  ", "40", "40", "40", "40", "40", "40", "40", "30",
						"30", "30" }, // l
				{ "M", "30", "30", "30", "40", "40", "40", "40", "40", "  ",
						"  ", "40", "40", "40", "40", "40", "40", "40", "30",
						"30", "30" }, // m
				{ "N", "30", "30", "30", "40", "40", "40", "40", "40", "  ",
						"  ", "40", "40", "40", "40", "40", "40", "40", "30",
						"30", "30" }, // n
				{ "O", "40", "40", "40", "50", "50", "50", "50", "50", "  ",
						"  ", "50", "50", "50", "50", "50", "50", "50", "40",
						"40", "40" }, // o
				{ "P", "40", "40", "40", "50", "50", "50", "50", "50", "  ",
						"  ", "50", "50", "50", "50", "50", "50", "50", "40",
						"40", "40" }, // p
				{ "Q", "40", "40", "40", "50", "50", "50", "50", "50", "50",
						"50", "50", "50", "50", "50", "50", "50", "50", "40",
						"40", "40" } }; // q

		buttonpanel = new JPanel();
		buttonpanel.setLayout(new GridLayout(16, 21));
		for (row = 0; row < ticketPrices.length; row++) {
			buttonlabel = new JLabel[16][21];
			for (column = 0; column < ticketPrices[row].length; column++) {
				buttonlabel[row][column] = new JLabel(ticketPrices[row][column]);
				buttonpanel.add(buttonlabel[row][column]);

			}
		}

		northpanel = new JPanel();
		northpanel.setBorder(new TitledBorder(new EtchedBorder(),
				"  Theater Management  "));
		northpanel.setLayout(new BorderLayout());
		northpanel.add(buttonpanel, BorderLayout.CENTER);

		choosepanel = new JPanel();
		rowbelabel = new JLabel(" Row (A - Q) :  ");
		rowbelabel.setFont(new Font(getName(), Font.BOLD, 25));
		rowbelabel.setHorizontalAlignment(SwingConstants.RIGHT);
		txtrow = new JTextField();
		// txtrow.setEditable(false);

		clomnlabel = new JLabel(" Column (1 - 20):  ");
		clomnlabel.setFont(new Font(getName(), Font.BOLD, 25));
		clomnlabel.setHorizontalAlignment(SwingConstants.RIGHT);
		txtclomn = new JTextField();
		// txtclomn.setEditable(false);

		submit = new JButton("Submit");
		submit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String a = String.valueOf(txtrow.getText());
				if (a.compareTo("A") == 0) {
					i = 1;
				} else if (a.compareTo("B") == 0) {
					i = 2;
				} else if (a.compareTo("C") == 0) {
					i = 3;
				} else if (a.compareTo("D") == 0) {
					i = 4;
				} else if (a.compareTo("E") == 0) {
					i = 5;
				} else if (a.compareTo("F") == 0) {
					i = 6;
				} else if (a.compareTo("G") == 0) {
					i = 7;
				} else if (a.compareTo("H") == 0) {
					i = 8;
				} else if (a.compareTo("I") == 0) {
					i = 9;
				} else if (a.compareTo("L") == 0) {
					i = 10;
				} else if (a.compareTo("M") == 0) {
					i = 11;
				} else if (a.compareTo("N") == 0) {
					i = 12;
				} else if (a.compareTo("O") == 0) {
					i = 13;
				} else if (a.compareTo("P") == 0) {
					i = 14;
				} else if (a.compareTo("Q") == 0) {
					i = 15;
				}
				j = Integer.valueOf(txtclomn.getText());
				theaterManagement.add(i, j);
				if (theaterManagement.seatAvailable(i, j) == false) {
					JOptionPane.showMessageDialog(null,
							"This seat is not available");
				}
				theaterManagement.changeAvailability(i, j);
				totalpricelabel.setText(String.valueOf(theaterManagement
						.totalPrice() + " Bath"));
				totalpricelabel.setFont(new Font(getName(), Font.BOLD, 25));
			}
		});
		choosepanel.setLayout(new GridLayout(1, 5));
		choosepanel.add(rowbelabel);
		choosepanel.add(txtrow);
		choosepanel.add(clomnlabel);
		choosepanel.add(txtclomn);
		choosepanel.add(submit);

		pricepenel = new JPanel();
		pricelabel = new JLabel(" search price :  ");
		pricelabel.setFont(new Font(getName(), Font.BOLD, 25));
		pricelabel.setHorizontalAlignment(SwingConstants.RIGHT);
		txtprice = new JTextField();
		seach = new JButton("Seach");
		seach.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				price = Integer.valueOf(txtprice.getText());
				JOptionPane.showMessageDialog(null,
						theaterManagement.showAvailableSeatsByPrice(price));
			}
		});
		totallabel = new JLabel(" total price :  ");
		totallabel.setFont(new Font(getName(), Font.BOLD, 20));
		totallabel.setHorizontalAlignment(SwingConstants.RIGHT);
		totalpricelabel = new JLabel(" 0 Bath");
		totalpricelabel.setFont(new Font(getName(), Font.BOLD, 25));
		pricepenel.setLayout(new GridLayout(1, 5));
		pricepenel.add(pricelabel);
		pricepenel.add(txtprice);
		pricepenel.add(seach);
		pricepenel.add(totallabel);
		pricepenel.add(totalpricelabel);

		southpanel = new JPanel();
		southpanel.setBorder(new TitledBorder(new EtchedBorder(), " Shop "));
		southpanel.setLayout(new GridLayout(2, 1));
		southpanel.add(choosepanel);
		southpanel.add(pricepenel);

		allpanel = new JPanel();
		allpanel.setLayout(new BorderLayout());
		allpanel.add(northpanel, BorderLayout.CENTER);
		allpanel.add(southpanel, BorderLayout.SOUTH);

		add(allpanel);
		setVisible(true);
	}
}
